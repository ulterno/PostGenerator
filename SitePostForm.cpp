#include "SitePostForm.h"
#include "ui_SitePostForm.h"
#include <QTextStream>
#include <QTimer>

#include <algorithm>

QString getInstallationDirectory (QString gemEnv);
QString getUserInstallationDirectory (QString gemEnv);

SitePostForm::SitePostForm(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::SitePostForm)
{
	QFile configFile ("config.json");
	if (!configFile.open(QFile::ReadOnly))
	{
		// Error
		exit(1);
	}
	m_config = QJsonDocument::fromJson(configFile.readAll()).object();
	configFile.close();
	ui->setupUi(this);
	QDir tempLoc (QDir::temp());
	tempLoc.mkpath(QApplication::applicationName());
	tempLoc.cd(QApplication::applicationName());
	programTempLocation = tempLoc.absolutePath() + "/";

	/// Read `gem environment`
	QProcess* gemEnvProc = new QProcess;
	gemEnvProc->setProgram("gem");
	gemEnvProc->setArguments(QStringList() << "environment");
	gemEnvProc->start();
	connect (gemEnvProc, &QProcess::readyReadStandardOutput, this, [this, gemEnvProc] ()
	{
		gem_environment = QString::fromUtf8(gemEnvProc->readAllStandardOutput());
		qDebug () << "Got stdout from \"gem\"\nStarting deleter";
		QTimer::singleShot(100, this, [gemEnvProc] ()
		{
			/// Note: Variable won't be deleted in case no stdout occured, furthermore, it might double delete in case this runs multiple times.
			/// Do something about it
			qDebug () << "Deleting unreferenced pointer of QProcess";
			delete gemEnvProc;
		});
	});
}

SitePostForm::~SitePostForm()
{
	delete ui;
}

void SitePostForm::on_pB_ReadInputFile_clicked()
{
	inputFileOperations();
}


void SitePostForm::on_pB_OpenFileDialogue_clicked()
{
	ui->lE_OpenFileLocation->setText(QFileDialog::getOpenFileUrl(this, "Open Markdown File").path());
	on_pB_ReadInputFile_clicked();
}

void SitePostForm::inputFileOperations()
{
	/// Clear text browser
	ui->textBrowser->setDisabled(true);
	ui->textBrowser->clear();

	/// Open input file
	QFile inputFile (ui->lE_OpenFileLocation->text());
	if (!inputFile.open(QFile::ReadOnly))
	{
		//Error
		exit (1);
	}
	QTextStream istr (&inputFile);

	/// Fill edited contents in browser
	QString previousLine;
	while (!istr.atEnd())
	{
		ui->textBrowser->insertPlainText(addBlocksIterationHelper(istr.readLine(), previousLine) + "\n");
	}
	ui->textBrowser->insertPlainText(addEndersForAllRemainingOpenBlocks());
	ui->textBrowser->setDisabled(false);
}

QString SitePostForm::addBlocksIterationHelper(const QString &currentLine, QString &previousLine)
{
	QString retVal;
	const QJsonArray headinLevelsArray = m_config["block"].toObject()["heading levels"].toArray();
	for (auto x : headinLevelsArray)
	{
		QJsonObject aLevel = x.toObject();
		int currentLevel = x.toObject()["level"].toInt();
		if (headingLevelsParsingInfo.contains(currentLevel) && headingLevelsParsingInfo[currentLevel])
		{	/// If block for this heading is open, check for end condition and close block
			const QJsonArray endConditionsArray = aLevel["end"].toArray();
			for (auto z : endConditionsArray)
			{	// Iterate through end conditions
				QString indicator = z.toObject()["indicator"].toString();
				if (currentLine.left(indicator.length()) == indicator)
				{	// If end condition indicator matches
					if ((!z.toObject().contains("break")) || (!z.toObject()["break"].toBool()))
					{	// No break condition
						continue;
					}
					if ((!z.toObject()["requires extra newline"].toBool()) || (previousLine.isEmpty() || previousLine.trimmed().isEmpty()))
					{	// If newline not required or newline exists, close block and add ender
						// headingLevelsParsingInfo.insert(currentLevel, false);
						headingLevelsParsingInfo.remove(currentLevel); // Better for addEndersForAllRemainingOpenBlocks()
						retVal += aLevel["block ender"].toString() + "\n";
						break;
					}
				}
			}
		}
		/// Look for block opening condition
		QString indicator = aLevel["indicator"].toString();
		if (currentLine.left(indicator.length()) == indicator)
		{	// If open condition indicator matches
			if ((!aLevel["requires extra newline"].toBool()) || (previousLine.isEmpty() || previousLine.trimmed().isEmpty()))
			{	// If newline not required or newline exists, open block and add opener
				headingLevelsParsingInfo.insert(currentLevel, true);
				retVal += aLevel["block opener"].toString() + "\n";
				endersList.insert(currentLevel, aLevel["block ender"].toString());
				break;
			}
		}
	}
	// m_config["block"].toObject()["heading levels"].toArray()

	retVal += currentLine;
	previousLine = currentLine;
	return retVal;
}

QString SitePostForm::addEndersForAllRemainingOpenBlocks()
{
	QString retVal;
	QList<int> headingLevels = headingLevelsParsingInfo.keys(true);
	std::sort(headingLevels.begin(), headingLevels.end(), [](const int& a, const int& b) -> bool
	{
		return (a > b);
	});
	const QJsonArray headinLevelsArray = m_config["block"].toObject()["heading levels"].toArray();
	for (auto keyHeadingLevel : qAsConst(headingLevels))
	{
		headingLevelsParsingInfo.remove(keyHeadingLevel);
		retVal += "\n" + endersList[keyHeadingLevel];
	}
	retVal += "\n";
	return retVal;
}


void SitePostForm::on_pB_SaveOutputToFile_clicked()
{
	QFile outFile;
	bool success = false;
	do
	{
		QUrl saveUrl = QFileDialog::getSaveFileUrl(this, "Save Edited Markdown File as ...");
		outFile.setFileName (saveUrl.path());
		success = outFile.open(QFile::ReadWrite);
		if (!success)
		{
			if (QMessageBox::critical(this, "File Opening Failed", "Failed opening file:\n" + outFile.errorString(), QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Cancel)
			{
				return;
			}
		}
	}
	while (!success);
	QString metaDataText;
	{
		metaDataText += "---\n";
		metaDataText += "layout: " + ui->lE_Layout->text() + "\n";
		metaDataText += "title: " + ui->lE_Title->text() + "\n";
		metaDataText += "date: " + ui->dte_Date->dateTime().toUTC().toString("yyyy-MM-dd HH:mm:ss") + "\n";
		metaDataText += "categories: " + ui->lE_Categories->text() + "\n";
		metaDataText += "author: [";
		for (auto authorLine : ui->pte_Author->toPlainText().split('\n'))
		{
			metaDataText += "\"" + authorLine + "\"" + ", ";
		}
		if (metaDataText.right(2) == ", ")
		{
			metaDataText.remove(metaDataText.length()-2, 2);
		}
		metaDataText += "]\n";
		metaDataText += "---\n";
	}
	if (outFile.write(metaDataText.toUtf8()) > 0)
	{
		if (outFile.write(ui->textBrowser->toPlainText().toUtf8()) > 0)
		{
			QMessageBox::information(this, "Data written successfully", "Data successfully written to : " + outFile.fileName());
		}
	}
}


void SitePostForm::on_pB_ViewHTMLOutput_clicked()
{
	QFile tempFile (programTempLocation + "infile_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss-zzz") + ".md");
	if (!tempFile.open(QFile::WriteOnly))
	{
		qDebug () << "Error Opening File : " << tempFile.fileName() << " Error: " << tempFile.errorString();
		return;
	}
	tempFile.write(ui->textBrowser->toPlainText().toUtf8());
	tempFile.close();
	if (!pp)
	{
		pp = new QProcess;
	}
	pp->setProgram(getUserInstallationDirectory(gem_environment) + "/bin/kramdown");
	pp->setArguments(QStringList() << "-i" << "kramdown" << "-o" << "html" << tempFile.fileName());
	connect(pp, &QProcess::readyReadStandardOutput, this, &SitePostForm::HTML_ToTextBrowser);
	pp->start();
}

void SitePostForm::on_pB_ViewHTMLInFirefox_clicked()
{
	QFile tempFile (programTempLocation + "infile_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss-zzz") + ".md");
	if (!tempFile.open(QFile::WriteOnly))
	{
		qDebug () << "Error Opening File : " << tempFile.fileName() << " Error: " << tempFile.errorString();
		return;
	}
	tempFile.write(ui->textBrowser->toPlainText().toUtf8());
	tempFile.close();
	if (!pp)
	{
		pp = new QProcess;
	}
	pp->setProgram(getUserInstallationDirectory(gem_environment) + "/bin/kramdown");
	ffOutFileName = programTempLocation + "outfile_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss-zzz") + ".html";
	pp->setArguments(QStringList() << "-i" << "kramdown" << "-o" << "html" << tempFile.fileName());
	connect(pp, &QProcess::readyReadStandardOutput, this, &SitePostForm::HTML_ToFirefox);
	pp->start();
}

void SitePostForm::HTML_ToTextBrowser()
{
	ui->textBrowser_2->clear();
	ui->textBrowser_2->append(QString::fromUtf8(pp->readAllStandardOutput()));
	disconnect(pp, &QProcess::readyReadStandardOutput, this, &SitePostForm::HTML_ToTextBrowser);
}

void SitePostForm::HTML_ToFirefox()
{
	disconnect(pp, &QProcess::readyReadStandardOutput, this, &SitePostForm::HTML_ToFirefox);
	QFile tempOutFile (ffOutFileName);
	if (!tempOutFile.open(QFile::WriteOnly))
	{
		qDebug () << "Error: Unable to write to file";
	}
	tempOutFile.write(pp->readAllStandardOutput());
	tempOutFile.close();
	QProcess::execute("xdg-open", QStringList() << ffOutFileName);
	qDebug () << ffOutFileName;
}

QString getInstallationDirectory (QString gemEnv)
{
	QTextStream as (&gemEnv);
	while (!as.atEnd())
	{
		QString aLine = as.readLine();
		if (aLine.contains("INSTALLATION DIRECTORY:"))
		{
			int indecx = aLine.indexOf(QLatin1String("INSTALLATION DIRECTORY:"));
			if (indecx < 0)
			{
				continue;
			}
			indecx += 24; // = number of characters in searched string
			return aLine.right(aLine.length() - indecx).trimmed();
		}
	}
	return QString{};
}
QString getUserInstallationDirectory (QString gemEnv)
{
	QTextStream as (&gemEnv);
	while (!as.atEnd())
	{
		QString aLine = as.readLine();
		if (aLine.contains("USER INSTALLATION DIRECTORY:"))
		{
			int indecx = aLine.indexOf(QLatin1String("USER INSTALLATION DIRECTORY:"));
			if (indecx < 0)
			{
				continue;
			}
			indecx += 29; // = number of characters in searched string
			return aLine.right(aLine.length() - indecx).trimmed();
		}
	}
	return QString{};
}

void SitePostForm::on_pB_dte_Date_clicked()
{
	ui->dte_Date->setDateTime(QDateTime::currentDateTime());
}

