#ifndef SITEPOSTFORM_H
#define SITEPOSTFORM_H

#include <QWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QPointer>
#include <QProcess>
#include <QStandardPaths>
#include <QDateTime>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui {
class SitePostForm;
}
QT_END_NAMESPACE

class SitePostForm : public QWidget
{
	Q_OBJECT

public:
	SitePostForm(QWidget *parent = nullptr);
	~SitePostForm();

private slots:
	void on_pB_ReadInputFile_clicked();

	void on_pB_OpenFileDialogue_clicked();

	void on_pB_SaveOutputToFile_clicked();

	void on_pB_ViewHTMLOutput_clicked();

	void on_pB_ViewHTMLInFirefox_clicked();

	void HTML_ToTextBrowser ();

	void HTML_ToFirefox ();
	void on_pB_dte_Date_clicked();

private:

	void inputFileOperations ();
	QString addBlocksIterationHelper(const QString& currentLine, QString& previousLine);
	QString addEndersForAllRemainingOpenBlocks ();

	QHash<int, bool> headingLevelsParsingInfo;
	QHash<int, QString> endersList;

	Ui::SitePostForm *ui;
	QJsonObject m_config;

	QPointer<QProcess> pp;
	QString ffOutFileName;
	QString programTempLocation;
	QString gem_environment;
};
#endif // SITEPOSTFORM_H
